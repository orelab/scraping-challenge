## Kitten scraping challenge ! ! !

### howto

First, have a database available from outside, with a table
of the following structure :

```sql
CREATE TABLE `chatons` (
  `url` varchar(255) PRIMARY KEY NOT NULL,
  `team` varchar(100) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);
```

Then, follow the next steps :

- npm install
- cp .env-sample .env
- configure .env
- run program with "npm run dev"
