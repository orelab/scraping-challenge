require('dotenv').config();


// DATABASE
// https://www.npmjs.com/package/mysql2

const DB = require('./DB');

const data = new DB(
  process.env.DBSERVER,
  process.env.DBUSER,
  process.env.DBPASS,
  process.env.DBNAME
);

data.connect().then(err => {
  if (err) return;
  
  let showStats = () => {
    data.detail().then(d => console.log);
  }
  // setInterval(showStats, process.env.REFRESH);
});


// WEBSERVER
// https://expressjs.com/fr/guide/routing.html

const express = require('express');
const app = express();

app.use(express.static('public')); // <- index.html

app.get('/api/stats', async (req, res) => {
  const [rows,fields] = await data.detail();
  return res.send(rows);
});

app.get('/api/scores', async (req, res) => {
  const [rows,fields] = await data.score();
  return res.send(rows);
});

app.listen(process.env.PORT, () => {
  console.log(`App listening at http://localhost:${process.env.PORT}`);
});



