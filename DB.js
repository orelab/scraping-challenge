var mysql = require('mysql2/promise');

module.exports = class DB {

  connection = {};
  host = '';
  user = '';
  pass = '';
  db = '';

  constructor(host, user, pass, db) {
    this.host = host;
    this.user = user;
    this.pass = pass;
    this.db = db;
  }
  
  async connect() {
    return this.connection = await mysql.createConnection({
      host: this.host,
      user: this.user,
      password: this.pass,
      database: this.db,
      rowsAsArray: true
    });
  }


  // On récupère les scrores de chaque teams
  async score() {
    return await this.connection.query(`
      SELECT team, COUNT(url) AS nb
      FROM chatons
      GROUP BY team;
    `);
  }
  

  // On compte le nombre d'occurences par teams et par minutes
  // dans l'optique de faire un graphique de l'évolution.
  async detail() {
    return await this.connection.query(`
      SELECT 
        DATE_FORMAT(created_time, '%Y-%m-%d %H:%i') AS time,
        team,
        COUNT(url) AS nb
      FROM chatons
      GROUP BY team, DATE_FORMAT(created_time, '%Y-%m-%d %H:%i');
    `);
  }

};
